package com.citi.training.staff.dao;

import com.citi.training.staff.model.Employee;

import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.repository.MongoRepository;

@Profile("MongoRepository")
public interface EmployeeMongoRepo extends EmployeeRepo, MongoRepository<Employee, String> {

}